package com.jantobola.spinningcube;

import com.badlogic.gdx.Gdx;
import com.jantobola.common.app.GameManager;
import com.jantobola.common.screen.ScreenManager;
import com.jantobola.common.screen.ScreenType;
import com.jantobola.spinningcube.screen.GameLevel;
import com.jantobola.spinningcube.screen.MainMenuScreen;
import com.jantobola.spinningcube.screen.SimpleLoadingProgressScreen;

public class Application extends GameManager {

	@Override
	public void createScreens(ScreenManager screenManager) {

		// Game screens
		screenManager.addScreen(ScreenType.GAME, new GameLevel(screenManager));

		// Menu screens
		screenManager.addScreen(ScreenType.MENU, new MainMenuScreen(screenManager));

		// Loading screens
		screenManager.addScreen(ScreenType.LOADING, new SimpleLoadingProgressScreen(screenManager, assetManager));

		// set default screen (type "MENU" with id "main") to be active
		screenManager.setDefaultScreen();
	}

	@Override
	public void render() {
		Gdx.gl.glClearColor(56 / 255.0f, 80 / 255.0f, 80 / 255.0f, 1);
		super.render();
	}
}
