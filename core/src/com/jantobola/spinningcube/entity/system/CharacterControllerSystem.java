package com.jantobola.spinningcube.entity.system;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.jantobola.common.entity.EntityEngine;
import com.jantobola.common.entity.component.mapper.Mapper;
import com.jantobola.common.entity.system.IteratingEntitySystem;
import com.jantobola.spinningcube.entity.component.CharacterComponent;
import com.jantobola.spinningcube.entity.component.MovingComponent;
import com.jantobola.spinningcube.entity.component.PhysicsComponent;

/**
 * CharacterControllerSystem
 *
 * @author Jan Tobola, 2015
 */
public class CharacterControllerSystem extends IteratingEntitySystem {

    @Mapper
    private ComponentMapper<PhysicsComponent> physics;

    @Mapper
    private ComponentMapper<MovingComponent> moving;

    private Camera camera;

    private Vector3 v = new Vector3(0, 0, 0);
    private Vector3 lastPos = new Vector3(0, 0, 0);

    private PhysicsComponent pc;
    private MovingComponent mc;

    private int rotateIndex = 1;

    int RIGHT = Input.Keys.RIGHT;
    int LEFT = Input.Keys.LEFT;
    int UP = Input.Keys.UP;
    int DOWN = Input.Keys.DOWN;

    public CharacterControllerSystem(Camera camera) {
        this.camera = camera;
    }

    @Override
    protected Family filter() {
        return Family.all(CharacterComponent.class, PhysicsComponent.class, MovingComponent.class).get();
    }

    @Override
    protected void addedCallback(EntityEngine engine) {
        InputMultiplexer mx = (InputMultiplexer) Gdx.input.getInputProcessor();
        mx.addProcessor(new InputProcessor() {

            @Override
            public boolean keyDown(int keycode) {
                switch (keycode) {
                    case Input.Keys.LEFT:
                    case Input.Keys.RIGHT:
                    case Input.Keys.UP:
                    case Input.Keys.DOWN:
                        mc.isMoving = true;
                        break;
                }

                return false;
            }

            @Override
            public boolean keyUp(int keycode) {
                switch (keycode) {
                    case Input.Keys.LEFT:
                    case Input.Keys.RIGHT:
                    case Input.Keys.UP:
                    case Input.Keys.DOWN:
                        mc.isMoving = false;
//                        stopMovement();
                        break;
                }

                return false;
            }

            private void stopMovement() {
                pc.body.clearForces();
                pc.body.setLinearVelocity(v.set(0, 0, 0));
                pc.body.setAngularVelocity(v.set(0, 0, 0));
            }

            @Override
            public boolean keyTyped(char character) {

                if(character == 'c') {
                    rotateCamera();
                }

                return false;
            }

            @Override
            public boolean touchDown(int screenX, int screenY, int pointer, int button) {
                return false;
            }

            @Override
            public boolean touchUp(int screenX, int screenY, int pointer, int button) {
                return false;
            }

            @Override
            public boolean touchDragged(int screenX, int screenY, int pointer) {
                return false;
            }

            @Override
            public boolean mouseMoved(int screenX, int screenY) {
                return false;
            }

            @Override
            public boolean scrolled(int amount) {
                return false;
            }
        });

        mx.addProcessor(new GestureDetector(new GestureDetector.GestureListener() {
            @Override
            public boolean touchDown(float x, float y, int pointer, int button) {
                return false;
            }

            @Override
            public boolean tap(float x, float y, int count, int button) {
                return false;
            }

            @Override
            public boolean longPress(float x, float y) {
                return false;
            }

            @Override
            public boolean fling(float velocityX, float velocityY, int button) {
                return false;
            }

            @Override
            public boolean pan(float x, float y, float deltaX, float deltaY) {
                System.out.println("X: " + x);
                System.out.println("Y: " + y);
                System.out.println("DX: " + deltaX);
                System.out.println("DY: " + deltaY);

                int w = Gdx.graphics.getWidth() / 2;
                if(x < w) {

                    // Movement

                    int movementSensitivity = 5;

                    switch (rotateIndex) {
                        case 1:

                            if(deltaY < 10) {
                                pc.body.applyCentralForce(v.set(deltaX * movementSensitivity, 0, 0));
                            }
                            if(deltaX < 10) {
                                pc.body.applyCentralForce(v.set(0, 0, deltaY * movementSensitivity));
                            }

                            break;
                        case 2:

                            if(deltaY < 10) {
                                pc.body.applyCentralForce(v.set(0, 0, -deltaX * movementSensitivity));
                            }
                            if(deltaX < 10) {
                                pc.body.applyCentralForce(v.set(deltaY * movementSensitivity, 0, 0));
                            }

                            break;
                        case 3:

                            if(deltaY < 10) {
                                pc.body.applyCentralForce(v.set(-deltaX * movementSensitivity, 0, 0));
                            }
                            if(deltaX < 10) {
                                pc.body.applyCentralForce(v.set(0, 0, -deltaY * movementSensitivity));
                            }

                            break;
                        case 4:

                            if(deltaY < 10) {
                                pc.body.applyCentralForce(v.set(0, 0, deltaX * movementSensitivity));
                            }
                            if(deltaX < 10) {
                                pc.body.applyCentralForce(v.set(-deltaY * movementSensitivity, 0, 0));
                            }

                            break;
                    }

                } else {
                    // Jump
                    pc.body.applyCentralImpulse(v.set(0, -deltaY / 20, 0));
                }

                return false;
            }

            @Override
            public boolean panStop(float x, float y, int pointer, int button) {


                return false;
            }

            @Override
            public boolean zoom(float initialDistance, float distance) {
                return false;
            }

            @Override
            public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2, Vector2 pointer1, Vector2 pointer2) {
                return false;
            }
        }));
    }

    @Override
    public void update(Entity entity, float delta) {
        pc = physics.get(entity);
        mc = moving.get(entity);

        if (mc.isMoving) {
//            pc.body.setAngularFactor(v.set(1, 0, 0));
//            pc.body.setAngularFactor(v.set(0, 1, 0));
        } else {
            pc.body.setLinearFactor(v.set(1, 1, 1));
            pc.body.setAngularFactor(v.set(1, 1, 1));
        }

        if (Gdx.input.isKeyPressed(RIGHT) && mc.isMoving) {
            pc.body.applyCentralForce(v.set(100, 0, 0));
        }

        if (Gdx.input.isKeyPressed(LEFT) && mc.isMoving) {
            pc.body.applyCentralForce(v.set(-100, 0, 0));
        }

        if (Gdx.input.isKeyPressed(UP) && mc.isMoving) {
            pc.body.applyCentralForce(v.set(0, 0, -100));
        }

        if (Gdx.input.isKeyPressed(DOWN) && mc.isMoving) {
            pc.body.applyCentralForce(v.set(0, 0, 100));
        }

        if (Gdx.input.isKeyJustPressed(Input.Keys.SPACE)) {
            pc.body.applyCentralImpulse(v.set(0, 10, 0));
        }

        lastPos = pc.body.getCenterOfMassPosition();

        switch (rotateIndex) {
            case 1:
                camera.position.set(lastPos.x, lastPos.y + 5, lastPos.z + 15);
                camera.lookAt(lastPos);
                camera.up.set(0, 1, 0);

                RIGHT = Input.Keys.RIGHT;
                LEFT = Input.Keys.LEFT;
                UP = Input.Keys.UP;
                DOWN = Input.Keys.DOWN;

                break;
            case 2:
                camera.position.set(lastPos.x + 15, lastPos.y + 5, lastPos.z);
                camera.lookAt(lastPos);
                camera.up.set(0, 1, 0);

                RIGHT = Input.Keys.DOWN;
                LEFT = Input.Keys.UP;
                UP = Input.Keys.RIGHT;
                DOWN = Input.Keys.LEFT;

                break;
            case 3:
                camera.position.set(lastPos.x, lastPos.y + 5, lastPos.z - 15);
                camera.lookAt(lastPos);
                camera.up.set(0, 1, 0);

                RIGHT = Input.Keys.LEFT;
                LEFT = Input.Keys.RIGHT;
                UP = Input.Keys.DOWN;
                DOWN = Input.Keys.UP;

                break;
            case 4:
                camera.position.set(lastPos.x - 15, lastPos.y + 5, lastPos.z);
                camera.lookAt(lastPos);
                camera.up.set(0, 1, 0);

                RIGHT = Input.Keys.UP;
                LEFT = Input.Keys.DOWN;
                UP = Input.Keys.LEFT;
                DOWN = Input.Keys.RIGHT;

                break;
        }

        camera.update(true);

    }

    public void rotateCamera() {
        rotateIndex++;
        if(rotateIndex > 4) {
            rotateIndex = 1;
        }
    }

    @Override
    protected void removedCallback(EntityEngine engine) {

    }
}
