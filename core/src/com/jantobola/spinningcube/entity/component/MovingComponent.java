package com.jantobola.spinningcube.entity.component;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.utils.Pool;

/**
 * MovingComponent
 *
 * @author Jan Tobola, 2015
 */
public class MovingComponent extends Component implements Pool.Poolable {

    public boolean isMoving = false;

    @Override
    public void reset() {
        isMoving = false;
    }

}
