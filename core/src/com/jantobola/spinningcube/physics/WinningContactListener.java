package com.jantobola.spinningcube.physics;

import com.badlogic.gdx.physics.bullet.collision.ContactListener;
import com.jantobola.spinningcube.screen.GameLevel;

/**
 * WinningContactListener
 *
 * @author Jan Tobola, 2015
 */
public class WinningContactListener extends ContactListener {

    private GameLevel level;

    public WinningContactListener(GameLevel level) {
        this.level = level;
    }

    @Override
    public boolean onContactAdded(int userValue0, int partId0, int index0, boolean match0, int userValue1, int partId1, int index1, boolean match1) {

        if(match0 || match1) {
            level.roundWin();
        }

        return true;
    }
}
