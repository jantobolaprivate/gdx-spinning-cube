package com.jantobola.spinningcube.screen;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.scenes.scene2d.ui.Value;
import com.jantobola.common.screen.ScreenManager;
import com.jantobola.common.screen.loadable.Screen2D;
import com.kotcrab.vis.ui.widget.VisProgressBar;
import com.kotcrab.vis.ui.widget.VisTable;

/**
 * SimpleLoadingProgressScreen
 *
 * @author Jan Tobola, 2015
 */
public class SimpleLoadingProgressScreen extends Screen2D {

    private AssetManager assetManager;

    private VisTable table;
    private VisProgressBar progress;

    public SimpleLoadingProgressScreen(ScreenManager screenManager, AssetManager assetManager) {
        super(screenManager);
        this.assetManager = assetManager;

        table = new VisTable(true);
        progress = new VisProgressBar(0, 1, 0.01f, false);

        stage.addActor(table);
        table.setFillParent(true);
        table.validate();

        table.add(progress).center().width(Value.percentWidth(0.7f).get(table));
    }

    @Override
    protected void requestAssets() {

    }

    @Override
    public void render(float delta) {

        float percentLoaded = assetManager.getProgress();
        progress.setValue(percentLoaded);

        super.render(delta);
    }
}
