package com.jantobola.spinningcube.screen;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Value;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.jantobola.common.screen.ScreenManager;
import com.jantobola.common.screen.ScreenType;
import com.jantobola.common.screen.loadable.Screen2D;
import com.kotcrab.vis.ui.widget.VisTable;
import com.kotcrab.vis.ui.widget.VisTextButton;

/**
 * MainMenuScreen
 *
 * @author Jan Tobola, 2015
 */
public class MainMenuScreen extends Screen2D {

	VisTable table = new VisTable();
	VisTextButton startBtn = new VisTextButton("START");
	VisTextButton quitBtn = new VisTextButton("QUIT");

	public MainMenuScreen(final ScreenManager screenManager) {
		super(screenManager);

		stage.addActor(table);
		table.setFillParent(true);
		table.validate();

		table.add(startBtn).width(Value.percentWidth(0.4f).get(table)).height(Value.percentHeight(0.2f).get(table));
		table.row();
		table.add(quitBtn).width(Value.percentWidth(0.4f).get(table)).height(Value.percentHeight(0.2f).get(table)).padTop(10);

		defineClicks();
	}

	private void defineClicks() {
		startBtn.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				screenManager.setScreen(ScreenType.GAME);
			}
		});

		quitBtn.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				screenManager.getGameManager().quit();
			}
		});
	}

	@Override
	protected void requestAssets() {

	}

}
