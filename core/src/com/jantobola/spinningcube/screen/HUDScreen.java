package com.jantobola.spinningcube.screen;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Value;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.jantobola.common.screen.ScreenManager;
import com.jantobola.common.screen.loadable.Screen2D;
import com.kotcrab.vis.ui.widget.VisLabel;
import com.kotcrab.vis.ui.widget.VisTable;
import com.kotcrab.vis.ui.widget.VisTextButton;

/**
 * HUDScreen
 *
 * @author Jan Tobola, 2015
 */
public class HUDScreen extends Screen2D {

	VisTable table = new VisTable();
	VisTable table2 = new VisTable();
	VisTable winTable = new VisTable();
	VisTable timeTable = new VisTable();

	VisTextButton pauseBtn = new VisTextButton("||");
	VisTextButton cameraBtn = new VisTextButton("C");

	VisTextButton winLabel = new VisTextButton("You WIN!");

	public VisLabel timeLabel = new VisLabel("", Color.WHITE);

	private GameLevel level;

	public HUDScreen(ScreenManager screenManager, GameLevel level) {
		super(screenManager);

		this.level = level;

		table.setFillParent(true);
		stage.addActor(table);

		table.validate();

		table.add(pauseBtn).width(Value.percentWidth(0.07f).get(table)).height(Value.percentHeight(0.09f).get(table));
		table.top().right().padTop(15).padRight(15);

		table2.setFillParent(true);
		stage.addActor(table2);

		table2.validate();

		table2.add(cameraBtn).width(Value.percentWidth(0.07f).get(table2)).height(Value.percentHeight(0.09f).get(table2));
		table2.bottom().right().padBottom(15).padRight(15);

		winTable.setFillParent(true);
		stage.addActor(winTable);
		winTable.validate();

		winTable.add(winLabel).width(Value.percentWidth(0.4f).get(winTable)).height(Value.percentHeight(0.3f).get(winTable));
		winTable.center();

		timeTable.setFillParent(true);
		stage.addActor(timeTable);
		timeTable.validate();

		timeTable.add(timeLabel);
		timeTable.top().left().padLeft(15).padTop(15);

		hideWinningBox();
		defineClicks();
	}

	private void defineClicks() {
		pauseBtn.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				screenManager.setDefaultScreen();
			}
		});

		cameraBtn.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				level.rotateCamera();
			}
		});
	}

	public void showWinningBox() {
		winTable.setVisible(true);
	}

	public void hideWinningBox() {
		winTable.setVisible(false);
	}

	@Override
	protected void requestAssets() {

	}
}
